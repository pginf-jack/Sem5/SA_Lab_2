# SA_Lab_2

Below you can find solution for the second required task of the agent system course.
More detailed explanation of what all the commands do can be found in the resources provided with the course.

---

`-Djava.security.properties=java.security` option extends settings from the default `java.security` file with the settings from a file named `java.security` in the folder that is currently open in your terminal (file with the required setting is part of this repository). This is required to enable encryption algorithms used by JADE that are disabled by default in newer versions of Java that you'll get when testing on your own computer. On laboratory computers, the default `java.security` file should already allow you to use these algorithms and usage of this option should not be needed (though it will not hurt anything either).

## Key pair generation

```sh
keytool -genkey -alias jade-main -keyalg RSA -keystore keystore-main -keypass changeit -storepass changeit -dname "CN=Giada,OU=JADE,O=FooBar Ltd,L=Naples,ST=na,C=IT"
keytool -genkey -alias jade-backupmain1 -keyalg RSA -keystore keystore-backupmain1 -keypass changeit -storepass changeit -dname "CN=Giada,OU=JADE,O=FooBar Ltd,L=Naples,ST=na,C=IT"
keytool -genkey -alias jade-backupmain2 -keyalg RSA -keystore keystore-backupmain2 -keypass changeit -storepass changeit -dname "CN=Giada,OU=JADE,O=FooBar Ltd,L=Naples,ST=na,C=IT"
keytool -genkey -alias jade-backupmain3 -keyalg RSA -keystore keystore-backupmain3 -keypass changeit -storepass changeit -dname "CN=Giada,OU=JADE,O=FooBar Ltd,L=Naples,ST=na,C=IT"
keytool -genkey -alias jade-container1 -keyalg RSA -keystore keystore-container1 -keypass changeit -storepass changeit -dname "CN=Giada,OU=JADE,O=FooBar Ltd,L=Naples,ST=na,C=IT"
keytool -genkey -alias jade-container2 -keyalg RSA -keystore keystore-container2 -keypass changeit -storepass changeit -dname "CN=Giada,OU=JADE,O=FooBar Ltd,L=Naples,ST=na,C=IT"
```

## Certificate export

```sh
keytool -export -keystore keystore-main -storepass changeit -alias jade-main -rfc -file main.cer
keytool -export -keystore keystore-backupmain1 -storepass changeit -alias jade-backupmain1 -rfc -file backupmain1.cer
keytool -export -keystore keystore-backupmain2 -storepass changeit -alias jade-backupmain2 -rfc -file backupmain2.cer
keytool -export -keystore keystore-backupmain3 -storepass changeit -alias jade-backupmain3 -rfc -file backupmain3.cer
keytool -export -keystore keystore-container1 -storepass changeit -alias jade-container1 -rfc -file container1.cer
keytool -export -keystore keystore-container2 -storepass changeit -alias jade-container2 -rfc -file container2.cer
```

## Certificate import

```sh
keytool -import -keystore truststore-main -storepass changeit -alias jade-backupmain1 -file backupmain1.cer -noprompt
keytool -import -keystore truststore-main -storepass changeit -alias jade-backupmain2 -file backupmain2.cer -noprompt
keytool -import -keystore truststore-main -storepass changeit -alias jade-backupmain3 -file backupmain3.cer -noprompt
keytool -import -keystore truststore-main -storepass changeit -alias jade-container1 -file container1.cer -noprompt
keytool -import -keystore truststore-main -storepass changeit -alias jade-container2 -file container2.cer -noprompt

keytool -import -keystore truststore-backupmain1 -storepass changeit -alias jade-main -file main.cer -noprompt
keytool -import -keystore truststore-backupmain1 -storepass changeit -alias jade-backupmain2 -file backupmain2.cer -noprompt
keytool -import -keystore truststore-backupmain1 -storepass changeit -alias jade-backupmain3 -file backupmain3.cer -noprompt
keytool -import -keystore truststore-backupmain1 -storepass changeit -alias jade-container1 -file container1.cer -noprompt
keytool -import -keystore truststore-backupmain1 -storepass changeit -alias jade-container2 -file container2.cer -noprompt

keytool -import -keystore truststore-backupmain2 -storepass changeit -alias jade-main -file main.cer -noprompt
keytool -import -keystore truststore-backupmain2 -storepass changeit -alias jade-backupmain1 -file backupmain1.cer -noprompt
keytool -import -keystore truststore-backupmain2 -storepass changeit -alias jade-backupmain3 -file backupmain3.cer -noprompt
keytool -import -keystore truststore-backupmain2 -storepass changeit -alias jade-container1 -file container1.cer -noprompt
keytool -import -keystore truststore-backupmain2 -storepass changeit -alias jade-container2 -file container2.cer -noprompt

keytool -import -keystore truststore-backupmain3 -storepass changeit -alias jade-main -file main.cer -noprompt
keytool -import -keystore truststore-backupmain3 -storepass changeit -alias jade-backupmain1 -file backupmain1.cer -noprompt
keytool -import -keystore truststore-backupmain3 -storepass changeit -alias jade-backupmain2 -file backupmain2.cer -noprompt
keytool -import -keystore truststore-backupmain3 -storepass changeit -alias jade-container1 -file container1.cer -noprompt
keytool -import -keystore truststore-backupmain3 -storepass changeit -alias jade-container2 -file container2.cer -noprompt

keytool -import -keystore truststore-container1 -storepass changeit -alias jade-main -file main.cer -noprompt
keytool -import -keystore truststore-container1 -storepass changeit -alias jade-backupmain1 -file backupmain1.cer -noprompt
keytool -import -keystore truststore-container1 -storepass changeit -alias jade-backupmain2 -file backupmain2.cer -noprompt
keytool -import -keystore truststore-container1 -storepass changeit -alias jade-backupmain3 -file backupmain3.cer -noprompt
keytool -import -keystore truststore-container1 -storepass changeit -alias jade-container2 -file container2.cer -noprompt

keytool -import -keystore truststore-container2 -storepass changeit -alias jade-main -file main.cer -noprompt
keytool -import -keystore truststore-container2 -storepass changeit -alias jade-backupmain1 -file backupmain1.cer -noprompt
keytool -import -keystore truststore-container2 -storepass changeit -alias jade-backupmain2 -file backupmain2.cer -noprompt
keytool -import -keystore truststore-container2 -storepass changeit -alias jade-backupmain3 -file backupmain3.cer -noprompt
keytool -import -keystore truststore-container2 -storepass changeit -alias jade-container1 -file container1.cer -noprompt
```

---

## Task 1 - Platform with encryption (no authorization) between two or three containers

```sh
java -Djava.security.properties=java.security -cp lib/jade.jar jade.Boot -port 5656 -local-port 5656 -container-name jade-main -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -gui m:jade.tools.DummyAgent.DummyAgent
java -Djava.security.properties=java.security -cp lib/jade.jar jade.Boot -port 5656 -local-port 4646 -container-name jade-container1 -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -container c1:jade.tools.DummyAgent.DummyAgent
```

## Task 2 - Platform with encryption and authorization between two or three containers

```sh
java -Djava.security.properties=java.security -Djavax.net.ssl.keyStore=keystore-main -Djavax.net.ssl.keyStorePassword=changeit -Djavax.net.ssl.trustStore=truststore-main -Djavax.net.ssl.trustStorePassword=changeit -cp lib/jade.jar jade.Boot -port 5656 -local-port 5656 -container-name jade-main -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -gui m:jade.tools.DummyAgent.DummyAgent
java -Djava.security.properties=java.security -Djavax.net.ssl.keyStore=keystore-container1 -Djavax.net.ssl.keyStorePassword=changeit -Djavax.net.ssl.trustStore=truststore-container1 -Djavax.net.ssl.trustStorePassword=changeit -cp lib/jade.jar jade.Boot -port 5656 -local-port 4646 -container-name jade-container1 -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -container c1:jade.tools.DummyAgent.DummyAgent
```

## Task 3 - Platform in star topology (without encryption or authorization) built using one main container, three backup containers, and two federated containers

```sh
java -cp lib/jade.jar jade.Boot -port 5656 -local-port 5656 -container-name jade-main -services jade.core.replication.MainReplicationService\;jade.core.replication.AddressNotificationService
java -cp lib/jade.jar jade.Boot -port 5656 -local-port 4646 -container-name jade-backupmain1 -services jade.core.replication.MainReplicationService\;jade.core.replication.AddressNotificationService -backupmain
java -cp lib/jade.jar jade.Boot -port 5656 -local-port 4647 -container-name jade-backupmain2 -services jade.core.replication.MainReplicationService\;jade.core.replication.AddressNotificationService -backupmain
java -cp lib/jade.jar jade.Boot -port 5656 -local-port 4648 -container-name jade-backupmain3 -services jade.core.replication.MainReplicationService\;jade.core.replication.AddressNotificationService -backupmain
java -cp lib/jade.jar jade.Boot -port 5656 -local-port 1616 -container-name jade-container1 -services jade.core.replication.AddressNotificationService -container -gui
java -cp lib/jade.jar jade.Boot -port 5656 -local-port 1617 -container-name jade-container2 -services jade.core.replication.AddressNotificationService -container

kill -9 "$(ps aux | grep '[j]ade-main' | awk '{print $2}')"
kill -9 "$(ps aux | grep '[j]ade-backupmain1' | awk '{print $2}')"
kill -9 "$(ps aux | grep '[j]ade-backupmain2' | awk '{print $2}')"
kill -9 "$(ps aux | grep '[j]ade-backupmain3' | awk '{print $2}')"
```

## Task 4 - Platform in star topology with encryption and authorization built using one main container, three backup containers, and two federated containers

```sh
java -Djava.security.properties=java.security -Djavax.net.ssl.keyStore=keystore-main -Djavax.net.ssl.keyStorePassword=changeit -Djavax.net.ssl.trustStore=truststore-main -Djavax.net.ssl.trustStorePassword=changeit -cp lib/jade.jar jade.Boot -port 5656 -local-port 5656 -container-name jade-main -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -services jade.core.replication.MainReplicationService\;jade.core.replication.AddressNotificationService m:jade.tools.DummyAgent.DummyAgent
java -Djava.security.properties=java.security -Djavax.net.ssl.keyStore=keystore-backupmain1 -Djavax.net.ssl.keyStorePassword=changeit -Djavax.net.ssl.trustStore=truststore-backupmain1 -Djavax.net.ssl.trustStorePassword=changeit -cp lib/jade.jar jade.Boot -port 5656 -local-port 4646 -container-name jade-backupmain1 -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -services jade.core.replication.MainReplicationService\;jade.core.replication.AddressNotificationService -backupmain b1:jade.tools.DummyAgent.DummyAgent
java -Djava.security.properties=java.security -Djavax.net.ssl.keyStore=keystore-backupmain2 -Djavax.net.ssl.keyStorePassword=changeit -Djavax.net.ssl.trustStore=truststore-backupmain2 -Djavax.net.ssl.trustStorePassword=changeit -cp lib/jade.jar jade.Boot -port 5656 -local-port 4647 -container-name jade-backupmain2 -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -services jade.core.replication.MainReplicationService\;jade.core.replication.AddressNotificationService -backupmain b2:jade.tools.DummyAgent.DummyAgent
java -Djava.security.properties=java.security -Djavax.net.ssl.keyStore=keystore-backupmain3 -Djavax.net.ssl.keyStorePassword=changeit -Djavax.net.ssl.trustStore=truststore-backupmain3 -Djavax.net.ssl.trustStorePassword=changeit -cp lib/jade.jar jade.Boot -port 5656 -local-port 4648 -container-name jade-backupmain3 -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -services jade.core.replication.MainReplicationService\;jade.core.replication.AddressNotificationService -backupmain b3:jade.tools.DummyAgent.DummyAgent
java -Djava.security.properties=java.security -Djavax.net.ssl.keyStore=keystore-container1 -Djavax.net.ssl.keyStorePassword=changeit -Djavax.net.ssl.trustStore=truststore-container1 -Djavax.net.ssl.trustStorePassword=changeit -cp lib/jade.jar jade.Boot -port 5656 -local-port 1616 -container-name jade-container1 -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -services jade.core.replication.AddressNotificationService -container -gui c1:jade.tools.DummyAgent.DummyAgent
java -Djava.security.properties=java.security -Djavax.net.ssl.keyStore=keystore-container2 -Djavax.net.ssl.keyStorePassword=changeit -Djavax.net.ssl.trustStore=truststore-container2 -Djavax.net.ssl.trustStorePassword=changeit -cp lib/jade.jar jade.Boot -port 5656 -local-port 1617 -container-name jade-container2 -nomtp -icps jade.imtp.leap.JICP.JICPSPeer -services jade.core.replication.AddressNotificationService -container c2:jade.tools.DummyAgent.DummyAgent

kill -9 "$(ps aux | grep '[j]ade-main' | awk '{print $2}')"
kill -9 "$(ps aux | grep '[j]ade-backupmain1' | awk '{print $2}')"
kill -9 "$(ps aux | grep '[j]ade-backupmain2' | awk '{print $2}')"
kill -9 "$(ps aux | grep '[j]ade-backupmain3' | awk '{print $2}')"
```
